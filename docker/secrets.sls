#!jinja|yaml|gpg

docker_gpg:
  container_user:
    password: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/XulGVTlC91lz4rXhDnWCd8FPZhx/+2Rc2HEe+1bpJKuC
      cXVrOK/dyzeupgUhTtRx/MsIAB6XtH90m0xbQoroJb4RG55YK9Vm+xesCxU2XB9B
      BopBbL+GJkMoWKWkUTSx06iWvf6qs7hQEe0Ig1UHCVJKzaxru7/g0AIXzs2NCUYw
      NyU5P5EvrC7hVVWAacgHnTdfdg6B3rdwK30LQVE0nCEoYsmGtEYzKHYvLPsFU5e9
      BLThd8VHyFc+AVmVugioNxqRPj8o7/NdDNIVCpef/BHLStvwOAx4w4xqRiXLAnSl
      CQ/RBFDP38n1OhN9Lheuw3x1OIhjQbmTwAfzr7Puy9JDARIFxHSnbf/u7v5cBjrX
      p9zdlGzY3xLT0LWNgRG0oeTLkGU8ZFQlpz5yYpkQhYybr4tHDodk3dWhJ0P0tKNN
      ATobww==
      =DsWo
      -----END PGP MESSAGE-----