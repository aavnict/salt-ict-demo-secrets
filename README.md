### Playbook description

- This repo for common state, we using gpg encrypt pillar, and basic reactor. Refer master configuration to execute the state

### Requirement:
- Install python-gnupg in Master and Minion, it's require by GPG.
### Master Configuration
- Clone this repo in folder /srv
- Import gpg public key and private key to gpg homedir ( Default in saltmaster : /etc/salt/gpgkeys)
    + mkdir -p /etc/salt/gpgkeys && chmod 0700 /etc/salt/gpgkeys
    + gpg --homedir /etc/salt/gpgkeys --import path/gpg_public.key
    + gpg --homedir /etc/salt/gpgkeys --import path/gpg_private.key
- Edit file /etc/salt/master:

```
fileserver_backend:
 - roots
 - gitfs
# This is for local state and pillar, comment this config if using remote state/pillar
file_roots:
   base:
     - /srv/salt-ict-demo/states/base	 
pillar_roots:
  base:
    - /srv/salt-ict-demo/pillars/base
# if using repo state and pillar.
gitfs_remotes:
  - git@bitbucket.org:aavnict/salt-ict-demo.git
    - root: states/base
gitfs_pubkey: "/root/.ssh/id_rsa.pub"
gitfs_privkey: "/root/.ssh/id_rsa"

ext_pillar:
  - git:
     - master git@bitbucket.org:aavnict/salt-ict-demo.git
	   - root: pillars/base
git_pillar_pubkey: "/root/.ssh/id_rsa.pub"
git_pillar_privkey: "/root/.ssh/id_rsa"

```
- For config reactor, create file /etc/salt/master.d/reactor.conf
```
reactor:
  - alert/admins/disk:
    - /srv/salt-ict-demo/states/base/reactor/disk_alert.sls
    - /srv/salt-ict-demo/states/base/reactor/highstate.sls


```
### How to run a state

```
     salt '*' state.sls common
	 or
	 salt '*' state.apply
```


### More detail
- See https://axiconsumer.atlassian.net/wiki/spaces/ICT/pages/581107800/KB+SaltStack
