#!yaml|gpg
ssl_certificate:
  key:
    content: |
     -----BEGIN PGP MESSAGE-----
     Version: GnuPG v2.0.22 (GNU/Linux)

     hQEMA86rrGwfdO21AQgAxuIvSkXYS5kx5/nxeAeQF08GuZVcoktmkbbnJC6CxDz1
     Y7RwWqKu5DVtHUxoA/hxRtSGzmNQcU6MEFKHwd3lZTEDfT1MfxKjhgJZwI5TJ+Qt
     RlI86M629uiJjXZ7NB4lensJO1voyb9ApCCvcTcZFDgcrp7LsRrMRch8R4Ascy4m
     QUAWG3R0FWt06gMb1QGbuRygqvrljOWEDTeWIpXuqZrK80uadza2Rgs5KO+korIW
     mrSdD8xr374LCcnGBJ33mH9UJHyTdZ7HSvjaH/5tEBbl+SkYYYwga/lvwhLP0eOK
     UJR6dFmpdyyTbRkzOvoUPuI7bUc/qq28p7w1epT5ZdLqASfcrwwETy0ivttzMnI0
     Nc6WaG+b7qMkLUv8JM69FNped9e34IbUM6KTDw5dA+hv1QI6vrd23WxvYbwqD13M
     VYI+TIo64eJLK0ggLknKnKji9QwN1aNdwKC1p8WP7FNZ+v1+cPoouZnAp0fgWJkq
     u32Cue+a7IwsJynWRElNqVQykrIwkCjSLdYvAY0j2L3bSmc1Y6N0Zj8k1zBzEIvz
     pP66rXAzNJd3FQlMEDQL5vbTTeqhggGt+0JiAXZeObZSDkFg2S2v/q3VsYTr9Iy2
     AMowqAtEZM/BxF60LDOzt/wj5l6t/2VIdrdr5is0d3ucYwkulgfFc0gPPztd1zGG
     MhcrahapvrNtt3GQB1XiO9VOLtQZmdl0OyvIkh0g2J79m6IUxrKIE8Q8fd0MZj8s
     8NQ/cZO7IRup6eVm220ayVPdI7kJCbndpiL63qNABxW1dafxm9tUgxWfww+7s0mr
     YddGpI8cieRYKq2BxQKw0SVEA8hMMYIgjYAydF6u6hOk2MlkZYsEPK1QyVaPeRgo
     GdQtPuuyG2d/V1GjYuOBZFPB8f/KFEFpJ9d2E/cu38ugvxRfEIUv3IJ/66U4GsWH
     lfpahsyRyZSDfPs7jPV7STONmMRKTwOvngdZlKTabpzYwGsDAQ+99WvQjHJG+D93
     lsDmzWaB40uF4telLdww1rSnnCc/3DrGDNajUb2lsqI5fQlfvdY8wGlBYII54VxY
     VRiuw45NWNHeRlvt0YzNGMlX7p9c9VZXq9pB7cwPayZBSJvzpNRD1NX/auPVbCX3
     1kPQBWwlQlTd4apvglqovmTQJkS93nW6eN1WPrSThTUXhcw3jkt5pDMbF3rzlJzP
     MCSUfbMaFGo1Nj1+aMOF6bVIutqFSIVObbtJhKacvD0RocNzkxBL/cDjHl4EuoQ0
     o9CLBV5/+b+LrS84H6Lah2AzSzl6L7FPwFBSwvGJVOYK+5HO0EYYczy+XCtcDc6J
     WI6uXvVHaWG72rBm8GVEImntJbkR7izwIREVQbE0+ZuL5G50yOHaGOx44aA1NyLm
     gE2q6lSciBy7GIlJ+uBWmMltsRLM6/sjqWIaxyzF/MsFOV32O+gEbBCmV2SJRa7l
     QWCbYMEVHY4VEPFU1i15l21IE/0hJz54OK2DnykNWnpFSEKcary4/UDKeDtbz/wC
     /o4qfK8IOo2G8/zPbTz1IeqzwTHObEzhet+nI7zRHO2Ky7I5t6LyzYwM8DYSAQnV
     na8xFpfOXBhr8FGzhcVjxxUN8eiIzGj4GXkurqzp6iMLTUZr9V0Bzz55wgov8/qk
     VdRyEkGxrKO93D1Up1iXGLWyHiTq5uTuPEBWI+6enqG+77FRmdx9t/PU2coFDjgB
     mcCNP+TVXhoZahg1UxlumSIQoois3NcdzNnk2nBih6l3bv9W0q9luAKQELZ3Unrw
     gkbA3af5g7iOq8mW/Nl2+BsOs5Em4/bujCOqFsYMprtN305Q1oOO7i4gOgS0XLWT
     QUMD6NEkijmDZ0XQbBKXZ7lF4CMB9eel4cPipn4i4p/ElDW9oRQ76WGKb/2FmW2I
     8zuvLOu+p4BVGRrrxw4/6jbmWaacM/oOSZ39YSyle+fre4ooQC5k3b00ySEMnog5
     SNM4lmnoUvf7PZX1QyXep3ouUiMjtcJxHVfg1fks/JJ6u4kVvxnsgwPnwp0cAFZG
     woFssWWp5qwMxzs/5Hehi877drCeglfFotgsBFDzj/nGXf1yn1X0LKeW0W3mdMeZ
     R2lVJqVsdr4qKE3OWuaMaG4aShtACpgzOoUXsOYdNXf8I/1Mb+UXXAjLIZwGfeKi
     =OYlv
     -----END PGP MESSAGE-----
  cert:
    content: |
     -----BEGIN PGP MESSAGE-----
     Version: GnuPG v2.0.22 (GNU/Linux)

     hQEMA86rrGwfdO21AQgArxivdh0tdBIsH3oHcjP8Kr8aiLTsNwtaQpGTea+qq4Y3
     BWReY+HX0T3q0pirl5dx4EtV4+x58aBJhU0fwMndOIIN+uPb5MQLJJxHuePEEjX9
     Ctix3Wyimz0x+3pZZ68YlcCKvvRjERgQFKfNGnCE3KQmM0mDkkR/dJhpI0f4St6c
     AwF+7sjtu8oynLxczAeOpDrjCUwgQNx6ABdpcHYDNU2miKiK9N6azNSSupKKqSzl
     dhjyfCQGlSACdmYOEJI7gR3EBYCIbuWeLIeIMFqz9k6UIeHc2wMrl2ZY4muBjsC5
     19b0BCmlKsjH4d6edKdgG3VmR8hLmuPY8P7kdAm61dLpAfdGqvnrqbJtb8OozThZ
     D1nCpbyPqSlI736G4M3aAt9nP1j79CDT7Duac4vjDB5pZBWnlU6bTHgTTUsfSYNo
     dR+EvZK57x2jpOYln4A7t0yXh8feKLgcFE2cK8mwwg4Gv7PPjpdsZkMZQFXBS+m0
     0qXb6PETT7qEtf6SrLvUg9vst8BJAU6vIWpvC9qJsGguRRV4JF4eogEwdSM3EGFs
     lclNQb2UGAnXIFvObL7AJ0eCLR7zE74biraOq+9fK3cI2kAHruZ6FfnJtZB+7wmK
     37O2FrF/cPirp9+rVAKwYGo+hGdEeNRiDYLsBGJpidOSTO3XS2HkomI7sk4fAbe1
     QV+sygCS01Sx1cm4kd2oJEcBIPivOoxIWWlLgr024L0chP2USlsf3IIjaaXDJJUF
     VBLsNOnuWcw8QZzm1uNsNExo4LHm4HK6rmh+Ew/461HjQLrs2cVH9B7OxrMasnqs
     ftCdxYAiHv10Y4SV+A6NAvhuOvTJyRLWuxk3czXqMZqYqKFxS8I0WkyAYULM2Ztx
     8NT5ErhoNDEB7mLeJKxosiiEiFzaXnFKMqE1fbH9LFI2Dn6NHYCIw86aSjKOiUD9
     qxk0Wx4UA6hoqObZUG+pllKgNfQSGqQqmx930f5FI6N1pgpzE31uHni/hMe2wAbl
     dE38Sx4RxjAtC61JS6m+aNrA4jVeMRXNym7QDkC19HTOYlD+3CS0mjUaPwJ5LCKG
     bJbCNDWeV/tZatb0flGoJ+JBs4/s5OdCQ73+/M3OQOU34DqgWATy8lyflFxDfPro
     weK2AdeopFAZINLCSFOKXQOWkjQuP6vb1LcluBlnw8PheRPHMWyditBfFUgWU0vO
     nyoUWcDFhXy+4KioDlVvddIXtrrUjZ59t0IKZ8IGq0SYp0/MB0mRMKE51PafikBS
     gPPQWx3pSXUd6XOlvwAX3SqSWZDQ5h8K9MygPRIvgdxvEfcUWLxtoi7htCDdWG9F
     P7rfk8oQ+ETwXmPX+EhRsfrPWCliZoPEK8+ikK1NClNtv3f4SoDTmdiERVx3Xofb
     t4iGnXIVMubo7D+TrdZR/Ue+sjvnfIH5X4SvzmbBDe+d68nrutBsf/ukBFinUvXj
     T5wuqjlW1gityKEZeg/hgk7yAQHp1L66/hmdwgC5cfjEPFGIrPQ+smuiHG1sSJBO
     n02h3QDVEcNWWmsaAZ8VqBFTR+vc4yExLb+PfoQPxP//0XPcRMlQmeV10N6P8lgV
     kFIWBok=
     =SQ+w
     -----END PGP MESSAGE-----
  fullchain:
    content: |
        -----BEGIN CERTIFICATE-----
        ZpL6Qc7zOlnEFCy5skawkRfgo7pxC4aX26I7PwT0WQlmSQIDAQABo4IDdjCCA3Iw
        JQYDVR0RBB4wHIINKi5heG9uaXZ5LmNvbYILYXhvbml2eS5jb20wCQYDVR0TBAIw
        ADArBgNVHR8EJDAiMCCgHqAchhpodHRwOi8vZ3Muc3ltY2IuY29tL2dzLmNybDBv
        BgNVHSAEaDBmMGQGBmeBDAECATBaMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5y
        YXBpZHNzbC5jb20vbGVnYWwwLAYIKwYBBQUHAgIwIAweaHR0cHM6Ly93d3cucmFw
        -----END CERTIFICATE-----
