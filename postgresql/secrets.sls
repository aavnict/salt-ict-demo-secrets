#!yaml|gpg
postgres:
  postgres_admin_password: |
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v2.0.22 (GNU/Linux)

    hQEMA86rrGwfdO21AQgAn8DxK0lZDTWBUnBBCj783o5r0N5lEZoL9DdiQWMtp9zV
    8j+g0xW8avgCSOQP9N6HbF2HQ0QcCdvi/2Hb+Zba+Ins3YG4TEJxctUejur9Zx63
    4+joZblLYiJsnuRx4x2UR1n80C2mKNfk8ZktFfO8/qQFhvK0+jIAZ8ROMY8Ck68e
    1VNbY/TD4q03/MB+TX2/zOqzSCGGSRzCttY3tH/pb0r9jRP0+Hrzk7AsepP6ybGs
    tIIbAqrLMl7j8sgBgV+8G9JpzawdHuoZHoT2OqVSV1Toq3ZoOLLODw33BLvrb/wS
    UzQi1jdEQCkHDtJGqa8p2eLEbzyGHefF4Rf5g2w+IdJDARtuj+Umn99zgi28vWR5
    uxKU39DLVs9xNga5K0t54jRUY22/oVxNFl8QJDGMWC2kADr3X3Q0X35BtWPc0S3x
    oNcE7w==
    =bR7d
    -----END PGP MESSAGE-----
  users:
   postgres4:
    password: |
     -----BEGIN PGP MESSAGE-----
     Version: GnuPG v2.0.22 (GNU/Linux)
     
     hQEMA86rrGwfdO21AQgAthsxzbt6AJNVicwji7tx0gsJK15S53xLL8FMptQnwXnf
     zDv5SVBoeUkKGVoS6dXVX2X7lGuHtn2l7NVmYD2ChS+MxInNGuQOZQJjDRgveiO6
     QnohBGWTtDNgettkyC7j0uQIQ/NZuw1hVtfb2VICIVG9cC41NbAnLVBGkzlxeaLK
     oUQHYkN/tOJuseKsylGe7EKmod1rcqq+ov7Iqq9iefmnP0uce7WdmHUKy4etf8I3
     Vv7enByjHD5QHtl7eQpFaurhM8hHpn7s2jSUr4Yb+bUIR43O6o0UJbykybOLbSyn
     H8BhIhbADf1vJS9IzetF8PtlR8sSsCNGNQ7Eai5fmtJDAcRYEyo5uAS9gX745BPz
     HPttDAwkkaaAhQY87dDE7F6m2w+/AkiLi7rSj5t3nyav4O5bLZBFtl6tzPAqapZW
     bdZVXg==
     =+3tH
     -----END PGP MESSAGE-----
   postgres5:
    password: |
     -----BEGIN PGP MESSAGE-----
     Version: GnuPG v2.0.22 (GNU/Linux)
     
     hQEMA86rrGwfdO21AQgAthsxzbt6AJNVicwji7tx0gsJK15S53xLL8FMptQnwXnf
     zDv5SVBoeUkKGVoS6dXVX2X7lGuHtn2l7NVmYD2ChS+MxInNGuQOZQJjDRgveiO6
     QnohBGWTtDNgettkyC7j0uQIQ/NZuw1hVtfb2VICIVG9cC41NbAnLVBGkzlxeaLK
     oUQHYkN/tOJuseKsylGe7EKmod1rcqq+ov7Iqq9iefmnP0uce7WdmHUKy4etf8I3
     Vv7enByjHD5QHtl7eQpFaurhM8hHpn7s2jSUr4Yb+bUIR43O6o0UJbykybOLbSyn
     H8BhIhbADf1vJS9IzetF8PtlR8sSsCNGNQ7Eai5fmtJDAcRYEyo5uAS9gX745BPz
     HPttDAwkkaaAhQY87dDE7F6m2w+/AkiLi7rSj5t3nyav4O5bLZBFtl6tzPAqapZW
     bdZVXg==
     =+3tH
     -----END PGP MESSAGE-----

