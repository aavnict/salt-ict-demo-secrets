#!jinja|yaml|gpg

{% set env = grains['environment'] %}

rundeck_gpg:
{% if env == 'dev' %}
  db_image:
    root_password: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/XulGVTlC91lz4rXhDnWCd8FPZhx/+2Rc2HEe+1bpJKuC
      cXVrOK/dyzeupgUhTtRx/MsIAB6XtH90m0xbQoroJb4RG55YK9Vm+xesCxU2XB9B
      BopBbL+GJkMoWKWkUTSx06iWvf6qs7hQEe0Ig1UHCVJKzaxru7/g0AIXzs2NCUYw
      NyU5P5EvrC7hVVWAacgHnTdfdg6B3rdwK30LQVE0nCEoYsmGtEYzKHYvLPsFU5e9
      BLThd8VHyFc+AVmVugioNxqRPj8o7/NdDNIVCpef/BHLStvwOAx4w4xqRiXLAnSl
      CQ/RBFDP38n1OhN9Lheuw3x1OIhjQbmTwAfzr7Puy9JDARIFxHSnbf/u7v5cBjrX
      p9zdlGzY3xLT0LWNgRG0oeTLkGU8ZFQlpz5yYpkQhYybr4tHDodk3dWhJ0P0tKNN
      ATobww==
      =DsWo
      -----END PGP MESSAGE-----

    user_password: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/XulGVTlC91lz4rXhDnWCd8FPZhx/+2Rc2HEe+1bpJKuC
      cXVrOK/dyzeupgUhTtRx/MsIAB6XtH90m0xbQoroJb4RG55YK9Vm+xesCxU2XB9B
      BopBbL+GJkMoWKWkUTSx06iWvf6qs7hQEe0Ig1UHCVJKzaxru7/g0AIXzs2NCUYw
      NyU5P5EvrC7hVVWAacgHnTdfdg6B3rdwK30LQVE0nCEoYsmGtEYzKHYvLPsFU5e9
      BLThd8VHyFc+AVmVugioNxqRPj8o7/NdDNIVCpef/BHLStvwOAx4w4xqRiXLAnSl
      CQ/RBFDP38n1OhN9Lheuw3x1OIhjQbmTwAfzr7Puy9JDARIFxHSnbf/u7v5cBjrX
      p9zdlGzY3xLT0LWNgRG0oeTLkGU8ZFQlpz5yYpkQhYybr4tHDodk3dWhJ0P0tKNN
      ATobww==
      =DsWo
      -----END PGP MESSAGE-----

  rundeck_image:
    rundeck_admin_password: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/XulGVTlC91lz4rXhDnWCd8FPZhx/+2Rc2HEe+1bpJKuC
      cXVrOK/dyzeupgUhTtRx/MsIAB6XtH90m0xbQoroJb4RG55YK9Vm+xesCxU2XB9B
      BopBbL+GJkMoWKWkUTSx06iWvf6qs7hQEe0Ig1UHCVJKzaxru7/g0AIXzs2NCUYw
      NyU5P5EvrC7hVVWAacgHnTdfdg6B3rdwK30LQVE0nCEoYsmGtEYzKHYvLPsFU5e9
      BLThd8VHyFc+AVmVugioNxqRPj8o7/NdDNIVCpef/BHLStvwOAx4w4xqRiXLAnSl
      CQ/RBFDP38n1OhN9Lheuw3x1OIhjQbmTwAfzr7Puy9JDARIFxHSnbf/u7v5cBjrX
      p9zdlGzY3xLT0LWNgRG0oeTLkGU8ZFQlpz5yYpkQhYybr4tHDodk3dWhJ0P0tKNN
      ATobww==
      =DsWo
      -----END PGP MESSAGE-----

    id_rsa: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf+J5ctgZ+eYZFykFF9bJVbIKlXxj2z40HtohGyzJ2JgkSk
      g8NOjtuu/MeKIfowBV5umHsTMq61sUE6cRtMWxWUSX0abOO+TcqhtWnVl25ykDyz
      B3yKLPLCTOs7xWvlNf2BcSpoUo6ZauziCOsrUd8qGkYSbSUfSeqKAkgJAed0U+KK
      dFW0xNI04Z3RvFzThMxfWNSzqW5ouapNEnOz1ZOXV+ZdtgLbmIa4tzWD97uqDcZY
      apV4DZFRSBAl0722XwwNlNP1pkSRqy22aDQLvx696KBfxGM/nCv7glOSfKg5wu8d
      MKvXODDGus7Q3p3jyOrU3piQ46eALTsNHpYvBpXoKNLrAdsV9p8bvKmMfH1q4veI
      yZp22lpkztDC8i3S74BtsSv5vR35KFFUSBHfSniFjTgWGpFW/BMVbRQkSgRtsla1
      UIAxlrEvBN+uu/SZtKEO1CL652yOE2gtq3bV5h8zI4lJj5nncuFpY95njudfLVRo
      RkuPvN0ZdLcRenBgqlyhKFIU5LSkbUFYFoTPhFjP03JxGmEqkHc/TZzXPxYWZCTQ
      m0yv6zl02bmtqo4PKcLS+fC9zAeO7KTkFEz9h3DXgJgdOQk42QyDo0vOUNQmk87l
      p7vLPKdunHAeEhY+rcExI3hfQlgq2e67SogLayb40oNo/drDUl15HSQIOrwOsCjp
      tSL6QOUwoIyoZ0tpvSJGxmeR9J5MTOwkzfbdvuGEGelqoWjUI5Rkt4VzCC8cS0Wo
      pJgDebU/+JrdW0dNoMiuflfe2h0bU/K9b77zEf2BFQMjP95zAwuLYzL4A07HQ5Vq
      R1BSK6hPiqKM80tSBYh3kaswVKa4JfKLpqrNBRlCjS55aZQzdKUoj1ET4zZ5eZ/t
      rIYA6j4Qt95/neN7HZJySqZj8fa68F759Jr112UqZMhz59kHNcRhKILMJW0ksFDY
      AUwI+XqmAWfDhSRgorIj9RuXfLPxbtfHyezNXg+evbeQHf24C4ZyUGhE9vUQ2DFE
      JoUqIERLJEZZMbYuhajMxNWGz31cYYjRnez7kDPl7Z9VL4IhCKS1ZlQbJLTOBNrK
      VkpeCGuRN7jI/4BhTsgkWuUhMGNhf32URmf/OYkbVgcL+GzANDYFMgQvqW+cIFzI
      ayFpi4YrjDLXLzTQK9k9UxOaoRkKxOeDiR1MUyYBZka1G//MCoI16INIsc5S1Ziy
      3EpuRdy6uq6JCebLi3+hVxBQRbIIdvv3YwOuH+IRwnFVQvpwXLBfCZYhH9Mzl0V3
      SYt/GA62KjUgJu5rul5swam1P+16DlfEWMs2X0M4vVK5PwkiBojFmMYkj4mjHDa3
      rxoaSxdxvQKvjFKV2ARla5lK3ttsBaxAp8zUkwUBdtw3nAx7YOrYNdSj+q4HNsSi
      x4yD8yRBFFGhFLrvE7urZl1ZnSn0Le5fkyvC/H9sB6mZn5Pe1nw8mgA5VZCtJQMU
      vDmvv8zmqsbemDNgzguLlzwp8sjOdA1neLJus2wa0UOV4YuBDSYDOsTrCalYKik+
      A/e/47mT4Fgq0PK1ATAOcjdqTcatL58BOvcy28V+ShBsZK6Af6bNGANQN8N5L+3b
      pDMdWs1fA1S5gtHR/yl43c0WDbwLKfwMjIqMKQiGjDMeUrMMwtUGbTZXoQtY49pC
      Pcr/rrNgrnbGiTUyQQkgTRxuGEbHRS2HF+kpDc9FhJqTl36WqwGtKX7/JGL6ZTeT
      yVxa4/FPuF5Sr41fdyLg0pcfGoIg0cD4uUoRCcQH/DLM7AGsqEQro6ySOvyEqWNs
      Vk6T3MpOpcBD5X3+arnBGZq5CzdGEmBWMujHYd9nLHEE2DF3MeduWLWI7stqC4vr
      c8Umkxt8Flz5nrlVXWss7+hZVbxRGZI/NJYEz8NwsE4h71L+Y6uJEjG0Ei4vdmbl
      AFM1R1+BtPp+YYSizHNEjkRLZAF7R40iX2hunBew7vJhAMHegclLt8FXH+z7LRrZ
      gYP+VjHyCzB/OUY0kkxPSB1k2lrvYKoysgeZDSJXQ5Jzp3WXDk6G4CaJMfEaR3wr
      9crtIlTEHsRgJPTToSk2OntZuDdWtXSMTSKxvaVdFIDtrdrKv7DoyOti5MIVB8L1
      UeZAxXP0mRNNbUBOSBdZcR7n7Y7myK4B3Xkx+5Nlxh7HPJYOPYY+6evMh1bGexBS
      vqLr84RYA/PL2L3oBYGQFkPkvd2I3OIomH7yeezBQqXfOCGp2WLhTa/SdGHGQdsS
      oawSXkJGdN91LRYmXvIEALpHcgl4HKHr0XvwwBwfk4oSIBlokcgPiYgyiZ2fpSSw
      rNaGEWlpkDsrmqb/zmPyUZitNu3PMxf1G1F8J0xoiuSQfE+gSPx4PNzm499diEOq
      7V2On/MF56RpK1jGIDxBqc7TdNRNMd2YpXF8BdvGtZWrGT0TeabiPV30/LE94jcO
      3N0BImNaRUAWXrosaabv0rRqdAaIz51NfwM1g05TKxLqCiq9acxhrIPlQK0AORAl
      4aEjpe+L3+SPVEJhGIaGIPvBCiKwDU/FIWFHOJg63mpdRdsmHmhruPqh3LTpNZSB
      BLW8bclfCka9XyhA8Yu1ZxqWG9jFZQdrqVmjWDRstAxwrd1onXgXO0ewqqn6isQB
      FZkzM6KyiJJxxXBWYc2ulrq4a7hslmlPTDzKOVEfLiPpBHQFdlJY/ctJtZKcVfQJ
      4MKTjq89LwBS3blR4Ce7IKcXzsXZNMMTF1pbPUIeiumCwqHPEJaeZxJt5yrGe5Jq
      dqvia6lPYBPBA4R6unC5dnKD+oQCxhIDCVcM5MZIPX3tIeCIfgq202PMKPpO2hEl
      uyYZG5Y/e2t4C1M+7MU2lmGonRqj4BwBrsJcz4S0eZBILOK3gs/gM+T2H1GHZ4xb
      rJe6XXhm+VaKNKZ0gGjYwpLuKfzzTQbnWtBK79lF/X9ojdN42Kwa2BsLGsR4ncaS
      xiHwPyVsiMXj5e+S3PrQigNqrC76hkOtdkKLgheK2T6Vpq30E7WTOh/qxo4uoi0k
      z+QiOVPuPcUTGRv3ea9GoPuEKMQV8g3tsZKDGFaj0k8eof1vn1egffREZV4Jed50
      8kfsJUNNUNLyu6MDQT79btvBNV1VT00s7GqWZtyyY1jab/QvVgn0RDOn5MHY12aX
      nnGQfzaa6WpDiOaSJPIykWlK7sYePBr2BAFg9ZWBj7j1027GCbX2sb8r3rUZ9GIg
      ZSqZB75ejzJ5qSP3GjZ4gk33cwWiVaxVZ9UuDZMeRz3fhPx5xwyz407Apu8nUT2g
      PFbBcU03iSh4ANH2YJGByzWlreNOydSWNooEaCsTtOMXxdSIEDoAjIaQGk6QCvTk
      GNRK2r0skU1+TY7ZlJPCKNl3zp9vBBce6+XzoLnqMQEV+NHlPaUi9Ypzrvlg69yZ
      rOGr9I9GEMLGeEYe4AEYbZ4aC39f8Xh8G5B+6rpS/idwNbXAb+eRmG5N7Utal3gf
      6Wj6gBKaY/M72Prkhix5oeLz1WDrCJP49MkJsuXdrZl/HbjMNRTCDgm3uwKiNRgz
      binLdwgALqIBhQJGxmGotBkPB9yA8sOSpnhdxX2lrUuyXvW0VdxtPlhdvZBdsOZN
      YhYFKDzuaXXJs6XvJPNotIJ27I2u7FdWdsmsbJ4fOW5K/gnd2hxjdlbPEdmABYPU
      oeiqnDEbpl3Tz0KKS36GTZgq0JEduVBYTa6TnLGflLIJu0wESCsVM2+lnPSCcSSo
      1ne/no/jAER4uMVr8Tw8P1BDLJSgwD1o2IarWtBTxgSfNwp+eeosTQ==
      =M/MK
      -----END PGP MESSAGE-----

{% elif env == 'prod' %}
  db_image:
    root_password: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/XulGVTlC91lz4rXhDnWCd8FPZhx/+2Rc2HEe+1bpJKuC
      cXVrOK/dyzeupgUhTtRx/MsIAB6XtH90m0xbQoroJb4RG55YK9Vm+xesCxU2XB9B
      BopBbL+GJkMoWKWkUTSx06iWvf6qs7hQEe0Ig1UHCVJKzaxru7/g0AIXzs2NCUYw
      NyU5P5EvrC7hVVWAacgHnTdfdg6B3rdwK30LQVE0nCEoYsmGtEYzKHYvLPsFU5e9
      BLThd8VHyFc+AVmVugioNxqRPj8o7/NdDNIVCpef/BHLStvwOAx4w4xqRiXLAnSl
      CQ/RBFDP38n1OhN9Lheuw3x1OIhjQbmTwAfzr7Puy9JDARIFxHSnbf/u7v5cBjrX
      p9zdlGzY3xLT0LWNgRG0oeTLkGU8ZFQlpz5yYpkQhYybr4tHDodk3dWhJ0P0tKNN
      ATobww==
      =DsWo
      -----END PGP MESSAGE-----

    user_password: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/XulGVTlC91lz4rXhDnWCd8FPZhx/+2Rc2HEe+1bpJKuC
      cXVrOK/dyzeupgUhTtRx/MsIAB6XtH90m0xbQoroJb4RG55YK9Vm+xesCxU2XB9B
      BopBbL+GJkMoWKWkUTSx06iWvf6qs7hQEe0Ig1UHCVJKzaxru7/g0AIXzs2NCUYw
      NyU5P5EvrC7hVVWAacgHnTdfdg6B3rdwK30LQVE0nCEoYsmGtEYzKHYvLPsFU5e9
      BLThd8VHyFc+AVmVugioNxqRPj8o7/NdDNIVCpef/BHLStvwOAx4w4xqRiXLAnSl
      CQ/RBFDP38n1OhN9Lheuw3x1OIhjQbmTwAfzr7Puy9JDARIFxHSnbf/u7v5cBjrX
      p9zdlGzY3xLT0LWNgRG0oeTLkGU8ZFQlpz5yYpkQhYybr4tHDodk3dWhJ0P0tKNN
      ATobww==
      =DsWo
      -----END PGP MESSAGE-----

  rundeck_image:
    rundeck_admin_password: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/XulGVTlC91lz4rXhDnWCd8FPZhx/+2Rc2HEe+1bpJKuC
      cXVrOK/dyzeupgUhTtRx/MsIAB6XtH90m0xbQoroJb4RG55YK9Vm+xesCxU2XB9B
      BopBbL+GJkMoWKWkUTSx06iWvf6qs7hQEe0Ig1UHCVJKzaxru7/g0AIXzs2NCUYw
      NyU5P5EvrC7hVVWAacgHnTdfdg6B3rdwK30LQVE0nCEoYsmGtEYzKHYvLPsFU5e9
      BLThd8VHyFc+AVmVugioNxqRPj8o7/NdDNIVCpef/BHLStvwOAx4w4xqRiXLAnSl
      CQ/RBFDP38n1OhN9Lheuw3x1OIhjQbmTwAfzr7Puy9JDARIFxHSnbf/u7v5cBjrX
      p9zdlGzY3xLT0LWNgRG0oeTLkGU8ZFQlpz5yYpkQhYybr4tHDodk3dWhJ0P0tKNN
      ATobww==
      =DsWo
      -----END PGP MESSAGE-----

    id_rsa: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf+J5ctgZ+eYZFykFF9bJVbIKlXxj2z40HtohGyzJ2JgkSk
      g8NOjtuu/MeKIfowBV5umHsTMq61sUE6cRtMWxWUSX0abOO+TcqhtWnVl25ykDyz
      B3yKLPLCTOs7xWvlNf2BcSpoUo6ZauziCOsrUd8qGkYSbSUfSeqKAkgJAed0U+KK
      dFW0xNI04Z3RvFzThMxfWNSzqW5ouapNEnOz1ZOXV+ZdtgLbmIa4tzWD97uqDcZY
      apV4DZFRSBAl0722XwwNlNP1pkSRqy22aDQLvx696KBfxGM/nCv7glOSfKg5wu8d
      MKvXODDGus7Q3p3jyOrU3piQ46eALTsNHpYvBpXoKNLrAdsV9p8bvKmMfH1q4veI
      yZp22lpkztDC8i3S74BtsSv5vR35KFFUSBHfSniFjTgWGpFW/BMVbRQkSgRtsla1
      UIAxlrEvBN+uu/SZtKEO1CL652yOE2gtq3bV5h8zI4lJj5nncuFpY95njudfLVRo
      RkuPvN0ZdLcRenBgqlyhKFIU5LSkbUFYFoTPhFjP03JxGmEqkHc/TZzXPxYWZCTQ
      m0yv6zl02bmtqo4PKcLS+fC9zAeO7KTkFEz9h3DXgJgdOQk42QyDo0vOUNQmk87l
      p7vLPKdunHAeEhY+rcExI3hfQlgq2e67SogLayb40oNo/drDUl15HSQIOrwOsCjp
      tSL6QOUwoIyoZ0tpvSJGxmeR9J5MTOwkzfbdvuGEGelqoWjUI5Rkt4VzCC8cS0Wo
      pJgDebU/+JrdW0dNoMiuflfe2h0bU/K9b77zEf2BFQMjP95zAwuLYzL4A07HQ5Vq
      R1BSK6hPiqKM80tSBYh3kaswVKa4JfKLpqrNBRlCjS55aZQzdKUoj1ET4zZ5eZ/t
      rIYA6j4Qt95/neN7HZJySqZj8fa68F759Jr112UqZMhz59kHNcRhKILMJW0ksFDY
      AUwI+XqmAWfDhSRgorIj9RuXfLPxbtfHyezNXg+evbeQHf24C4ZyUGhE9vUQ2DFE
      JoUqIERLJEZZMbYuhajMxNWGz31cYYjRnez7kDPl7Z9VL4IhCKS1ZlQbJLTOBNrK
      VkpeCGuRN7jI/4BhTsgkWuUhMGNhf32URmf/OYkbVgcL+GzANDYFMgQvqW+cIFzI
      ayFpi4YrjDLXLzTQK9k9UxOaoRkKxOeDiR1MUyYBZka1G//MCoI16INIsc5S1Ziy
      3EpuRdy6uq6JCebLi3+hVxBQRbIIdvv3YwOuH+IRwnFVQvpwXLBfCZYhH9Mzl0V3
      SYt/GA62KjUgJu5rul5swam1P+16DlfEWMs2X0M4vVK5PwkiBojFmMYkj4mjHDa3
      rxoaSxdxvQKvjFKV2ARla5lK3ttsBaxAp8zUkwUBdtw3nAx7YOrYNdSj+q4HNsSi
      x4yD8yRBFFGhFLrvE7urZl1ZnSn0Le5fkyvC/H9sB6mZn5Pe1nw8mgA5VZCtJQMU
      vDmvv8zmqsbemDNgzguLlzwp8sjOdA1neLJus2wa0UOV4YuBDSYDOsTrCalYKik+
      A/e/47mT4Fgq0PK1ATAOcjdqTcatL58BOvcy28V+ShBsZK6Af6bNGANQN8N5L+3b
      pDMdWs1fA1S5gtHR/yl43c0WDbwLKfwMjIqMKQiGjDMeUrMMwtUGbTZXoQtY49pC
      Pcr/rrNgrnbGiTUyQQkgTRxuGEbHRS2HF+kpDc9FhJqTl36WqwGtKX7/JGL6ZTeT
      yVxa4/FPuF5Sr41fdyLg0pcfGoIg0cD4uUoRCcQH/DLM7AGsqEQro6ySOvyEqWNs
      Vk6T3MpOpcBD5X3+arnBGZq5CzdGEmBWMujHYd9nLHEE2DF3MeduWLWI7stqC4vr
      c8Umkxt8Flz5nrlVXWss7+hZVbxRGZI/NJYEz8NwsE4h71L+Y6uJEjG0Ei4vdmbl
      AFM1R1+BtPp+YYSizHNEjkRLZAF7R40iX2hunBew7vJhAMHegclLt8FXH+z7LRrZ
      gYP+VjHyCzB/OUY0kkxPSB1k2lrvYKoysgeZDSJXQ5Jzp3WXDk6G4CaJMfEaR3wr
      9crtIlTEHsRgJPTToSk2OntZuDdWtXSMTSKxvaVdFIDtrdrKv7DoyOti5MIVB8L1
      UeZAxXP0mRNNbUBOSBdZcR7n7Y7myK4B3Xkx+5Nlxh7HPJYOPYY+6evMh1bGexBS
      vqLr84RYA/PL2L3oBYGQFkPkvd2I3OIomH7yeezBQqXfOCGp2WLhTa/SdGHGQdsS
      oawSXkJGdN91LRYmXvIEALpHcgl4HKHr0XvwwBwfk4oSIBlokcgPiYgyiZ2fpSSw
      rNaGEWlpkDsrmqb/zmPyUZitNu3PMxf1G1F8J0xoiuSQfE+gSPx4PNzm499diEOq
      7V2On/MF56RpK1jGIDxBqc7TdNRNMd2YpXF8BdvGtZWrGT0TeabiPV30/LE94jcO
      3N0BImNaRUAWXrosaabv0rRqdAaIz51NfwM1g05TKxLqCiq9acxhrIPlQK0AORAl
      4aEjpe+L3+SPVEJhGIaGIPvBCiKwDU/FIWFHOJg63mpdRdsmHmhruPqh3LTpNZSB
      BLW8bclfCka9XyhA8Yu1ZxqWG9jFZQdrqVmjWDRstAxwrd1onXgXO0ewqqn6isQB
      FZkzM6KyiJJxxXBWYc2ulrq4a7hslmlPTDzKOVEfLiPpBHQFdlJY/ctJtZKcVfQJ
      4MKTjq89LwBS3blR4Ce7IKcXzsXZNMMTF1pbPUIeiumCwqHPEJaeZxJt5yrGe5Jq
      dqvia6lPYBPBA4R6unC5dnKD+oQCxhIDCVcM5MZIPX3tIeCIfgq202PMKPpO2hEl
      uyYZG5Y/e2t4C1M+7MU2lmGonRqj4BwBrsJcz4S0eZBILOK3gs/gM+T2H1GHZ4xb
      rJe6XXhm+VaKNKZ0gGjYwpLuKfzzTQbnWtBK79lF/X9ojdN42Kwa2BsLGsR4ncaS
      xiHwPyVsiMXj5e+S3PrQigNqrC76hkOtdkKLgheK2T6Vpq30E7WTOh/qxo4uoi0k
      z+QiOVPuPcUTGRv3ea9GoPuEKMQV8g3tsZKDGFaj0k8eof1vn1egffREZV4Jed50
      8kfsJUNNUNLyu6MDQT79btvBNV1VT00s7GqWZtyyY1jab/QvVgn0RDOn5MHY12aX
      nnGQfzaa6WpDiOaSJPIykWlK7sYePBr2BAFg9ZWBj7j1027GCbX2sb8r3rUZ9GIg
      ZSqZB75ejzJ5qSP3GjZ4gk33cwWiVaxVZ9UuDZMeRz3fhPx5xwyz407Apu8nUT2g
      PFbBcU03iSh4ANH2YJGByzWlreNOydSWNooEaCsTtOMXxdSIEDoAjIaQGk6QCvTk
      GNRK2r0skU1+TY7ZlJPCKNl3zp9vBBce6+XzoLnqMQEV+NHlPaUi9Ypzrvlg69yZ
      rOGr9I9GEMLGeEYe4AEYbZ4aC39f8Xh8G5B+6rpS/idwNbXAb+eRmG5N7Utal3gf
      6Wj6gBKaY/M72Prkhix5oeLz1WDrCJP49MkJsuXdrZl/HbjMNRTCDgm3uwKiNRgz
      binLdwgALqIBhQJGxmGotBkPB9yA8sOSpnhdxX2lrUuyXvW0VdxtPlhdvZBdsOZN
      YhYFKDzuaXXJs6XvJPNotIJ27I2u7FdWdsmsbJ4fOW5K/gnd2hxjdlbPEdmABYPU
      oeiqnDEbpl3Tz0KKS36GTZgq0JEduVBYTa6TnLGflLIJu0wESCsVM2+lnPSCcSSo
      1ne/no/jAER4uMVr8Tw8P1BDLJSgwD1o2IarWtBTxgSfNwp+eeosTQ==
      =M/MK
      -----END PGP MESSAGE-----
{% else %}
  none: none
{% endif %}