#!yaml|gpg

bastion_gpg:
  s3:
    access_key: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQf/RXC5v6vPBuRWKU52/I8CbJy3mQUUHo7y5dmVJBWuEJIS
      oDLfBWGnGeppBD7fqLWgoD4X3rHqJ74WdkCagIyCyi8kjcdeEVY+ILPF+iMaOnq6
      XPzp9qeHncUsaHX9BlVL+5f1zqMgIvv1MpOkm+0myDdm/fmwjq6gzTFUnYKZIR39
      suLayZD+67d/wH/8owiMFE+wP3NapL6s0nkpOia4u4LORiaoFjSrlrFp1dpCxSiX
      yg86HkrrXaP2ZTKW4hxd02KuE3URCZ+wiEAHTWkAz5qt1sIKYceGjkRYj7JvteO/
      gzgQ2kHQydwBAofFBPuUVCGEk1d3Gz9p2T0wO4nsUtJPAZ1y/1jpXywegBiKTQmy
      ifbGxOa/rqpvF322joNwyPZS1x/Mie1v/3D4ykJ0cdbDMtT+c0oqWSj5z5VZJ3Vb
      KDHbWmUT6B4U1sTnx97WVg==
      =NaL4
      -----END PGP MESSAGE-----
    secret_key: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQgAnJmj7Pt7g6lc/Vg1SO8hhWVrVeFdPQN1DuRmYBp4wq1l
      dG2kb9DehLGAn2msMk5lfvYL3ZH+lUdBhKzwp8xj5HuU+q5S9vt84CqY+FOEqZ4G
      xkrpZpiGidWGLhmZutpomNCgLI8+wb7b/hZv7Dj++p/o8CYAXkH0E7pyffiMqs06
      lIuxZL3KlIzmH9UkLf5gyTTGsa4nJdppy3kSHjgmMuESkqwdGjl2yE8ipvbtFIPz
      YJMOXUmXnmvJlIOh1i0CyIDknWk4TrA0sxSvqkqdtCSWZaQjE3Q+d7fkXfN5gnVR
      PXMToKfIxI1fZpTFXGGxKvfXHoJRpqPbCq5HfVGNatJjAcCneRieRDSU1Mufee1Z
      4VAxfT9FRmTKEp/C6vSzchEH7gnRWWwn0H0ZnLGnHNPIjditlrox6zkqyMh5wkiO
      YknF1wblcBAs8cXPSNRR4yeemh2Yj5wRUTGlNdb8MvTCSB3/
      =/dEi
      -----END PGP MESSAGE-----
  gg_2fa:
    cloud_user: |
      -----BEGIN PGP MESSAGE-----
      Version: GnuPG v2.0.22 (GNU/Linux)

      hQEMA86rrGwfdO21AQgArOKP1t4EjSR92cZQzJxqhtDqsw2Z2zFgAeApHs6RJA/X
      27BAjzBdTx3yoje8uMw3JR7UuyylmOR3LbzIVIy/fMf/ZFjQoTxDMPz/BlYc+QFF
      4LL/BQIt1KNwEUkPoKtHZ3/c/k4Akh9y0jWdoBNH06e78o0NIsU4ye7cwJo+A1xB
      WehSyaJZBUFL0/fM1SYL1yTKf6k+Q8Rd+LQpAy+S0vl+3M+6nlCNroAvC7NN34+f
      Wht+B0EzfQq8YgVpzbyDSurJBFZnjErNIUoJLHDb0t3U+FLbAkSFUw3OBwWBkoMS
      QpND13adQojYfn2FdPBe9/L/gizadFAPYFjRI6wWq9LACwH3BilyvAsE3hXBgv3q
      qKVSPHMPnkPagVYsnjEs5X/km3tFZNkRYEs2Xb/FWYKZ8HwrZtGMlKpBamXqlZ8o
      988R6rcQ2mEuzz8Rb98mHx37qhWWViFQE6Cqt5rewGuCw6C32OmAdSzdVwBaiWDM
      0KGJsYsMc+JOTqkx/B35aUqlKT6H/7weruIL6PNYWMzhJ1kbE7br4OR+6R4knHa5
      yaL0tQH0Shx1C2eftbT/tyEU6I5R/dCBHyXxCr+bqX6+fo/avR9hwqd7xkP2
      =6R5m
      -----END PGP MESSAGE-----